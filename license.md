# classic.mp3

Licensed under the EFF OAL License

http://freemusicarchive.org/music/Advent_Chamber_Orchestra/Selections_from_the_2005-2006_Season/Advent_Chamber_Orchestra_-_04_-_Mozart_-_A_Little_Night_Music_allegro


# win.mp3 win.wav loose.mp3 loose.wav

Licensed under Attribution 3.0 Unported (CC BY 3.0) 

https://freesound.org/people/Mativve/sounds/391539/

https://freesound.org/people/Mativve/sounds/391536/

https://creativecommons.org/licenses/by/3.0/

